/*
Copyright 2016 Aviral Dasgupta
Copyright 2016 OpenMarket Ltd
Copyright 2017-2020 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import NovaBasePlatform from './NovaBasePlatform';
import dis from 'matrix-react-sdk/src/dispatcher/dispatcher';
import {_t} from 'matrix-react-sdk/src/languageHandler';
import {Room} from "matrix-js-sdk/src/models/room";

import url from 'url';
import UAParser from 'ua-parser-js';

export default class WebPlatform extends NovaBasePlatform {
    getHumanReadableName(): string {
        return 'Web Platform'; // no translation required: only used for analytics
    }

    /**
     * Returns true if the platform supports displaying
     * notifications, otherwise false.
     */
    supportsNotifications(): boolean {
        return Boolean(window.Notification);
    }

    /**
     * Returns true if the application currently has permission
     * to display notifications. Otherwise false.
     */
    maySendNotifications(): boolean {
        return window.Notification.permission === 'granted';
    }

    /**
     * Requests permission to send notifications. Returns
     * a promise that is resolved when the user has responded
     * to the request. The promise has a single string argument
     * that is 'granted' if the user allowed the request or
     * 'denied' otherwise.
     */
    requestNotificationPermission(): Promise<string> {
        // annoyingly, the latest spec says this returns a
        // promise, but this is only supported in Chrome 46
        // and Firefox 47, so adapt the callback API.
        return new Promise(function(resolve, reject) {
            window.Notification.requestPermission((result) => {
                resolve(result);
            });
        });
    }

    displayNotification(title: string, msg: string, avatarUrl: string, room: Room, requireInteraction: boolean) {
        const notifBody = {
            body: msg,
            tag: "vector",
            silent: true, // we play our own sounds,
            // requireInteraction,
        };
        if (avatarUrl) notifBody['icon'] = avatarUrl;
        const notification = new window.Notification(title, notifBody);

        notification.onclick = function() {
            dis.dispatch({
                action: 'view_room',
                room_id: room.roomId,
            });
            window.focus();
            if (window.todesktop && window.todesktop.custom.showMainWindow) {
                window.todesktop.custom.showMainWindow();
            }
            notification.close();
        };
        // Chrome only dismisses notifications after 20s, which
        // is waaaaay too long
        if (!requireInteraction) {
            const duration = 5;
            global.setTimeout(function() {
                notification.close();
            }, duration * 1000);
        }
    }

    getDefaultDeviceDisplayName(): string {
        // strip query-string and fragment from uri
        const u = url.parse(window.location.href);
        u.protocol = "";
        u.search = "";
        u.hash = "";
        // Remove trailing slash if present
        u.pathname = u.pathname.replace(/\/$/, "");

        let appName = u.format();
        // Remove leading slashes if present
        appName = appName.replace(/^\/\//, "");
        // `appName` is now in the format `develop.element.io`.

        const ua = new UAParser();
        const browserName = ua.getBrowser().name || "unknown browser";
        let osName = ua.getOS().name || "unknown OS";
        // Stylise the value from the parser to match Apple's current branding.
        if (osName === "Mac OS") osName = "macOS";
        return _t('%(appName)s (%(browserName)s, %(osName)s)', {
            appName,
            browserName,
            osName,
        });
    }

    screenCaptureErrorString(): string | null {
        // it won't work at all if you're not on HTTPS so whine whine whine
        if (window.location.protocol !== "https:") {
            return _t("You need to be using HTTPS to place a screen-sharing call.");
        }
        return null;
    }

    reload() {
        // forceReload=false since we don't really need new HTML/JS files
        // we just need to restart the JS runtime.
        window.location.reload(false);
    }
}
