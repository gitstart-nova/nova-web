/*
Copyright 2020 Nova Technology Ltd.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import BasePlatform, {UpdateCheckStatus} from 'matrix-react-sdk/src/BasePlatform';
import {_t} from 'matrix-react-sdk/src/languageHandler';
import request from 'browser-request';
import {getVectorConfig} from "../getconfig";

import Favicon from "../../favicon";
import {hideToast as hideUpdateToast, showToast as showUpdateToast} from "matrix-react-sdk/src/toasts/UpdateToast";
import dis from "matrix-react-sdk/src/dispatcher/dispatcher";
import {CheckUpdatesPayload} from "matrix-react-sdk/src/dispatcher/payloads/CheckUpdatesPayload";
import {Action} from "matrix-react-sdk/src/dispatcher/actions";

export const updateCheckStatusEnum = {
    CHECKING: 'CHECKING',
    ERROR: 'ERROR',
    NOTAVAILABLE: 'NOTAVAILABLE',
    DOWNLOADING: 'DOWNLOADING',
    READY: 'READY',
};

const POKE_RATE_MS = 10 * 60 * 1000; // 10 min
/**
 * Nova-specific extensions to the BasePlatform template
 */
export default abstract class NovaBasePlatform extends BasePlatform {
    protected _favicon: Favicon;
    protected pipelineId: string = null;

    async getConfig(): Promise<{}> {
        return getVectorConfig();
    }

    getHumanReadableName(): string {
        return 'Nova Base Platform'; // no translation required: only used for analytics
    }

    /**
     * Delay creating the `Favicon` instance until first use (on the first notification) as
     * it uses canvas, which can trigger a permission prompt in Firefox's resist fingerprinting mode.
     * See https://github.com/vector-im/riot-web/issues/9605.
     */
    get favicon() {
        if (this._favicon) {
            return this._favicon;
        }
        return this._favicon = new Favicon();
    }

    _updateFavicon() {
        let bgColor = "#d00";
        let notif: string | number = this.notificationCount;

        if (this.errorDidOccur) {
            notif = notif || "×";
            bgColor = "#f00";
        }

        this.favicon.badge(notif, { bgColor });
    }

    setNotificationCount(count: number) {
        if (this.notificationCount === count) return;
        super.setNotificationCount(count);
        this._updateFavicon();
    }

    setErrorStatus(errorDidOccur: boolean) {
        if (this.errorDidOccur === errorDidOccur) return;
        super.setErrorStatus(errorDidOccur);
        this._updateFavicon();
    }

    /**
     * Get a sensible default display name for the
     * device Vector is running on
     */
    getDefaultDeviceDisplayName(): string {
        return _t("Unknown device");
    }

    _getLastPipeline(): Promise<string> {
        return new Promise(function(resolve, reject) {
            request(
                {
                    method: "GET",
                    url: "https://gitlab.com/api/v4/projects/14987503/pipelines",
                    qs: {
                        order_by: "updated_at",
                        status: "success"
                    },
                },
                (err, response, body) => {
                    if (err || response.status !== 200) {
                        if (err === null) err = { status: response.status };
                        reject(err);
                        return;
                    }

                    const pipelines = JSON.parse(body) || [];
                    const id = pipelines[0].id;
                    if (id) {
                        resolve("" + id);
                    }
                    reject();
                },
            );
        });
    }

    startUpdater() {
        this.pollForUpdate();
        setInterval(this.pollForUpdate, POKE_RATE_MS);
    }

    async canSelfUpdate(): Promise<boolean> {
        return true;
    }

    pollForUpdate = () => {
        return this._getLastPipeline().then((id) => {
            if (this.pipelineId === null) {
                this.pipelineId = id;
            } else if (this.pipelineId !== id) {
                if (this.shouldShowUpdate(id)) {
                    showUpdateToast(this.pipelineId, id);
                }
                return { status: UpdateCheckStatus.Ready };
            } else {
                hideUpdateToast();
            }

            return { status: UpdateCheckStatus.NotAvailable };
        }, (err) => {
            console.error("Failed to poll for update", err);
            return {
                status: UpdateCheckStatus.Error,
                detail: err.message || err.status ? err.status.toString() : 'Unknown Error',
            };
        });
    };

    startUpdateCheck() {
        super.startUpdateCheck();
        this.pollForUpdate().then((updateState) => {
            dis.dispatch<CheckUpdatesPayload>({
                action: Action.CheckUpdates,
                ...updateState,
            });
        });
    }

    installUpdate() {
        window.location.reload(true);
    }

    getAppVersion(): Promise<string> {
        return Promise.resolve(null);
    }
}
